

function renderEmailsList(emails,wrapperID) {
    const emailListDom = emails.map(creatEmailItem);
    const container = document.getElementById(wrapperID)

    container.addEventListener('click',openMailText);
    document.getElementById(wrapperID).append(...emailListDom);

}

function creatEmailItem(emailItem) {
    const emailDomElem = document.createElement('div');
    const emailSubject = document.createElement('p');
    const emailFrom = document.createElement('p');
    const emailTo = document.createElement('p');
    const emailText = document.createElement('p');

    emailDomElem.append(emailSubject,emailFrom,emailTo,emailText);
    emailText.hidden = true ;

    emailSubject.innerText = emailItem.subject;
    emailFrom.innerText = emailItem.from;
    emailTo.innerText = emailItem.to;
    emailText.innerText = emailItem.text;

    emailDomElem.className = 'email-item';
    emailSubject.className = 'email-subject';
    emailFrom.className = 'email-from';
    emailTo.className = 'email-to';
    emailText.className = 'email-text';

    return emailDomElem
}

function openMailText(event) {
    const emailItem =  event.path.find(item => {
        return item.classList.contains('email-item');
    })
    const emailText = emailItem.getElementsByClassName('email-text')[0];

    const textAll = [...document.getElementsByClassName('email-text')].filter(t => t !== emailText);

    textAll.forEach(text => text.hidden = true)

    emailText.hidden = !emailText.hidden
}

const mailStorage = [
    { subject : 'ello world' ,
        from: 'aaa' ,
        to : 'bbb' ,
        text :'some text 1' },
    { subject : 'ello world' ,
        from: 'aaa' ,
        to : 'bbb' ,
        text :'some text 2' },
    { subject : 'ello world' ,
        from: 'aaa' ,
        to : 'bbb' ,
        text :'some text 3' }
]


creatEmailItem(mailStorage)
renderEmailsList(mailStorage,'mailList')
